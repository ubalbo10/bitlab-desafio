package sv.edu.bitlab.desafio.jorge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.firestore.FirebaseFirestore
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val firestoreReference = FirebaseFirestore.getInstance()

        firestoreReference.collection("accounts").addSnapshotListener { querySnapshot, firebaseFirestoreException ->
            querySnapshot?.forEach { item ->
                Log.i("data", "response -> ${item.data}")
            }
        }



    }
}
